import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

    public static void main(String[] args){
        ArrayList<Integer> negNum = new ArrayList<Integer>();
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        if (args.length == 0) {
            System.out.println("0");
            System.exit(0);
        }

        boolean isNum = args[0].matches(".*\\d+.*");
        if (isNum == false) {
            System.out.println("0");
            System.exit(0);
        }

        //Find all numbers;
        Matcher matcher = Pattern.compile("-?\\d*\\.?\\d+").matcher(args[0]);
        while (matcher.find()) {
            numbers.add(Integer.parseInt(matcher.group()));
        }


        for(int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) < 0){
                negNum.add(numbers.get(i));
            }
        }

        if (negNum.size() > 0){
            System.out.println("“negatives not allowed: " + Arrays.toString(negNum.toArray()));
            System.exit(0);
        }

        else {

            double sum = Add(args[0]);
            System.out.println(sum);
        }


    }

    public static Integer Add(String numbers) {

        int out = 0;
        for(int i = 0; i < numbers.length() ; i++){
            if( Character.isDigit(numbers.charAt(i)) ){
                out = out + Character.getNumericValue(numbers.charAt(i));
            }
        }
        return out;


    }
}

